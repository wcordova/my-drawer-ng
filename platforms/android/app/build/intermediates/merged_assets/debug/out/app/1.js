(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[1],{

/***/ "./app/featured/featured-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturedRoutingModule", function() { return FeaturedRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@nativescript/angular/__ivy_ngcc__/fesm2015/nativescript-angular.js");
/* harmony import */ var _featured_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/featured/featured.component.ts");





const routes = [
    { path: "", component: _featured_component__WEBPACK_IMPORTED_MODULE_2__["FeaturedComponent"] }
];
class FeaturedRoutingModule {
}
FeaturedRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: FeaturedRoutingModule });
FeaturedRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function FeaturedRoutingModule_Factory(t) { return new (t || FeaturedRoutingModule)(); }, imports: [[_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)], _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](FeaturedRoutingModule, { imports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]], exports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FeaturedRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
                exports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./app/featured/featured.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturedComponent", function() { return FeaturedComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _nativescript_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@nativescript/core/index.js");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/@nativescript/angular/__ivy_ngcc__/fesm2015/nativescript-angular.js");




class FeaturedComponent {
    constructor() {
        // Use the component constructor to inject providers.
    }
    ngOnInit() {
        // Init your component properties here.
    }
    onDrawerButtonTap() {
        const sideDrawer = _nativescript_core__WEBPACK_IMPORTED_MODULE_1__["Application"].getRootView();
        sideDrawer.showDrawer();
    }
}
FeaturedComponent.ɵfac = function FeaturedComponent_Factory(t) { return new (t || FeaturedComponent)(); };
FeaturedComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FeaturedComponent, selectors: [["Featured"]], decls: 7, vars: 0, consts: [[0, "ios", "visibility", "collapsed", "icon", "res://menu", 3, "tap"], ["icon", "res://menu", 0, "android", "visibility", "collapsed", "ios.position", "left", 3, "tap"], ["text", "Featured"], [1, "page__content"], ["text", "\uF005", 1, "page__content-icon", "fas"], ["text", "<!-- Page content goes here -->", 1, "page__content-placeholder"]], template: function FeaturedComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ActionBar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "NavigationButton", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("tap", function FeaturedComponent_Template_NavigationButton_tap_1_listener() { return ctx.onDrawerButtonTap(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ActionItem", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("tap", function FeaturedComponent_Template_ActionItem_tap_2_listener() { return ctx.onDrawerButtonTap(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "Label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "GridLayout", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "Label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "Label", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_2__["ActionBarComponent"], _nativescript_angular__WEBPACK_IMPORTED_MODULE_2__["NavigationButtonDirective"], _nativescript_angular__WEBPACK_IMPORTED_MODULE_2__["ActionItemDirective"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FeaturedComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: "Featured",
                templateUrl: "./featured.component.html"
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./app/featured/featured.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturedModule", function() { return FeaturedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@nativescript/angular/__ivy_ngcc__/fesm2015/nativescript-angular.js");
/* harmony import */ var _featured_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/featured/featured-routing.module.ts");
/* harmony import */ var _featured_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/featured/featured.component.ts");





class FeaturedModule {
}
FeaturedModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: FeaturedModule });
FeaturedModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function FeaturedModule_Factory(t) { return new (t || FeaturedModule)(); }, imports: [[
            _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
            _featured_routing_module__WEBPACK_IMPORTED_MODULE_2__["FeaturedRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](FeaturedModule, { declarations: [_featured_component__WEBPACK_IMPORTED_MODULE_3__["FeaturedComponent"]], imports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
        _featured_routing_module__WEBPACK_IMPORTED_MODULE_2__["FeaturedRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FeaturedModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                    _featured_routing_module__WEBPACK_IMPORTED_MODULE_2__["FeaturedRoutingModule"]
                ],
                declarations: [
                    _featured_component__WEBPACK_IMPORTED_MODULE_3__["FeaturedComponent"]
                ],
                schemas: [
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvZmVhdHVyZWQvZmVhdHVyZWQtcm91dGluZy5tb2R1bGUudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL2ZlYXR1cmVkL2ZlYXR1cmVkLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9hcHAvZmVhdHVyZWQvZmVhdHVyZWQuY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vYXBwL2ZlYXR1cmVkL2ZlYXR1cmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXlDO0FBRXdCO0FBRVI7OztBQUV6RCxNQUFNLE1BQU0sR0FBVztJQUNuQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLHFFQUFpQixFQUFFO0NBQzdDLENBQUM7QUFNSyxNQUFNLHFCQUFxQjs7b0dBQXJCLHFCQUFxQjtvS0FBckIscUJBQXFCLGtCQUhyQixDQUFDLDhFQUF3QixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUMxQyw4RUFBd0I7bUlBRXpCLHFCQUFxQix5R0FGcEIsOEVBQXdCOzZGQUV6QixxQkFBcUI7Y0FKakMsc0RBQVE7ZUFBQztnQkFDTixPQUFPLEVBQUUsQ0FBQyw4RUFBd0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3BELE9BQU8sRUFBRSxDQUFDLDhFQUF3QixDQUFDO2FBQ3RDOzs7Ozs7Ozs7O0FDYkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFrRDtBQUVEOzs7QUFNMUMsTUFBTSxpQkFBaUI7SUFFMUI7UUFDSSxxREFBcUQ7SUFDekQsQ0FBQztJQUVELFFBQVE7UUFDSix1Q0FBdUM7SUFDM0MsQ0FBQztJQUVELGlCQUFpQjtRQUNiLE1BQU0sVUFBVSxHQUFrQiw4REFBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzVELFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUM1QixDQUFDOztrRkFiUSxpQkFBaUI7aUdBQWpCLGlCQUFpQjtRQ1I5Qiw0RUFDSTtRQUlBLHNGQUE4RztRQUEvQywrSUFBTyx1QkFBbUIsSUFBQztRQUFDLDREQUFtQjtRQU05RyxnRkFFYTtRQUZnRCx5SUFBTyx1QkFBbUIsSUFBQztRQUV4Riw0REFBYTtRQUNiLHNFQUErQjtRQUNuQyw0REFBWTtRQUVaLGdGQUNJO1FBQUEsc0VBQThEO1FBQzlELHNFQUF3RjtRQUM1Riw0REFBYTs7NkZEWkEsaUJBQWlCO2NBSjdCLHVEQUFTO2VBQUM7Z0JBQ1AsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLFdBQVcsRUFBRSwyQkFBMkI7YUFDM0M7Ozs7Ozs7Ozs7QUVQRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMkQ7QUFDTTtBQUVDO0FBQ1Q7O0FBY2xELE1BQU0sY0FBYzs7NkZBQWQsY0FBYztzSkFBZCxjQUFjLGtCQVhkO1lBQ0wsOEVBQXdCO1lBQ3hCLDhFQUFxQjtTQUN4QjttSUFRUSxjQUFjLG1CQU5uQixxRUFBaUIsYUFKakIsOEVBQXdCO1FBQ3hCLDhFQUFxQjs2RkFTaEIsY0FBYztjQVoxQixzREFBUTtlQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCw4RUFBd0I7b0JBQ3hCLDhFQUFxQjtpQkFDeEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNWLHFFQUFpQjtpQkFDcEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLDhEQUFnQjtpQkFDbkI7YUFDSiIsImZpbGUiOiIxLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJAbmF0aXZlc2NyaXB0L2FuZ3VsYXJcIjtcclxuXHJcbmltcG9ydCB7IEZlYXR1cmVkQ29tcG9uZW50IH0gZnJvbSBcIi4vZmVhdHVyZWQuY29tcG9uZW50XCI7XHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICAgIHsgcGF0aDogXCJcIiwgY29tcG9uZW50OiBGZWF0dXJlZENvbXBvbmVudCB9XHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcclxuICAgIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGZWF0dXJlZFJvdXRpbmdNb2R1bGUgeyB9XHJcbiIsImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUmFkU2lkZURyYXdlciB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlclwiO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbiB9IGZyb20gXCJAbmF0aXZlc2NyaXB0L2NvcmVcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwiRmVhdHVyZWRcIixcclxuICAgIHRlbXBsYXRlVXJsOiBcIi4vZmVhdHVyZWQuY29tcG9uZW50Lmh0bWxcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmVhdHVyZWRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIC8vIFVzZSB0aGUgY29tcG9uZW50IGNvbnN0cnVjdG9yIHRvIGluamVjdCBwcm92aWRlcnMuXHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgLy8gSW5pdCB5b3VyIGNvbXBvbmVudCBwcm9wZXJ0aWVzIGhlcmUuXHJcbiAgICB9XHJcblxyXG4gICAgb25EcmF3ZXJCdXR0b25UYXAoKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3Qgc2lkZURyYXdlciA9IDxSYWRTaWRlRHJhd2VyPkFwcGxpY2F0aW9uLmdldFJvb3RWaWV3KCk7XHJcbiAgICAgICAgc2lkZURyYXdlci5zaG93RHJhd2VyKCk7XHJcbiAgICB9XHJcbn1cclxuIiwiPEFjdGlvbkJhcj5cclxuICAgIDwhLS0gXHJcbiAgICBVc2UgdGhlIE5hdmlnYXRpb25CdXR0b24gYXMgYSBzaWRlLWRyYXdlciBidXR0b24gaW4gQW5kcm9pZFxyXG4gICAgYmVjYXVzZSBBY3Rpb25JdGVtcyBhcmUgc2hvd24gb24gdGhlIHJpZ2h0IHNpZGUgb2YgdGhlIEFjdGlvbkJhclxyXG4gICAgLS0+XHJcbiAgICA8TmF2aWdhdGlvbkJ1dHRvbiBpb3M6dmlzaWJpbGl0eT1cImNvbGxhcHNlZFwiIGljb249XCJyZXM6Ly9tZW51XCIgKHRhcCk9XCJvbkRyYXdlckJ1dHRvblRhcCgpXCI+PC9OYXZpZ2F0aW9uQnV0dG9uPlxyXG4gICAgPCEtLSBcclxuICAgIFVzZSB0aGUgQWN0aW9uSXRlbSBmb3IgSU9TIHdpdGggcG9zaXRpb24gc2V0IHRvIGxlZnQuIFVzaW5nIHRoZVxyXG4gICAgTmF2aWdhdGlvbkJ1dHRvbiBhcyBhIHNpZGUtZHJhd2VyIGJ1dHRvbiBpbiBpT1MgaXMgbm90IHBvc3NpYmxlLFxyXG4gICAgYmVjYXVzZSBpdHMgZnVuY3Rpb24gaXMgdG8gYWx3YXlzIG5hdmlnYXRlIGJhY2sgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gICAgLS0+XHJcbiAgICA8QWN0aW9uSXRlbSBpY29uPVwicmVzOi8vbWVudVwiIGFuZHJvaWQ6dmlzaWJpbGl0eT1cImNvbGxhcHNlZFwiICh0YXApPVwib25EcmF3ZXJCdXR0b25UYXAoKVwiXHJcbiAgICAgICAgaW9zLnBvc2l0aW9uPVwibGVmdFwiPlxyXG4gICAgPC9BY3Rpb25JdGVtPlxyXG4gICAgPExhYmVsIHRleHQ9XCJGZWF0dXJlZFwiPjwvTGFiZWw+XHJcbjwvQWN0aW9uQmFyPlxyXG5cclxuPEdyaWRMYXlvdXQgY2xhc3M9XCJwYWdlX19jb250ZW50XCI+XHJcbiAgICA8TGFiZWwgY2xhc3M9XCJwYWdlX19jb250ZW50LWljb24gZmFzXCIgdGV4dD1cIiYjeGYwMDU7XCI+PC9MYWJlbD5cclxuICAgIDxMYWJlbCBjbGFzcz1cInBhZ2VfX2NvbnRlbnQtcGxhY2Vob2xkZXJcIiB0ZXh0PVwiPCEtLSBQYWdlIGNvbnRlbnQgZ29lcyBoZXJlIC0tPlwiPjwvTGFiZWw+XHJcbjwvR3JpZExheW91dD4iLCJpbXBvcnQgeyBOZ01vZHVsZSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJAbmF0aXZlc2NyaXB0L2FuZ3VsYXJcIjtcclxuXHJcbmltcG9ydCB7IEZlYXR1cmVkUm91dGluZ01vZHVsZSB9IGZyb20gXCIuL2ZlYXR1cmVkLXJvdXRpbmcubW9kdWxlXCI7XHJcbmltcG9ydCB7IEZlYXR1cmVkQ29tcG9uZW50IH0gZnJvbSBcIi4vZmVhdHVyZWQuY29tcG9uZW50XCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSxcclxuICAgICAgICBGZWF0dXJlZFJvdXRpbmdNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBGZWF0dXJlZENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtcclxuICAgICAgICBOT19FUlJPUlNfU0NIRU1BXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGZWF0dXJlZE1vZHVsZSB7IH1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ==