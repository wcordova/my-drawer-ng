(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[3],{

/***/ "./app/search/search-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchRoutingModule", function() { return SearchRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@nativescript/angular/__ivy_ngcc__/fesm2015/nativescript-angular.js");
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/search/search.component.ts");





const routes = [
    { path: "", component: _search_component__WEBPACK_IMPORTED_MODULE_2__["SearchComponent"] }
];
class SearchRoutingModule {
}
SearchRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: SearchRoutingModule });
SearchRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function SearchRoutingModule_Factory(t) { return new (t || SearchRoutingModule)(); }, imports: [[_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)], _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SearchRoutingModule, { imports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]], exports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
                exports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _nativescript_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@nativescript/core/index.js");
/* harmony import */ var _domian_noticias_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/domian/noticias.service.ts");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/@nativescript/angular/__ivy_ngcc__/fesm2015/nativescript-angular.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");







function SearchComponent_Label_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "Label", 6);
} if (rf & 2) {
    const s_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("text", s_r1);
} }
class SearchComponent {
    constructor(noticias) {
        this.noticias = noticias;
        // Use the component constructor to inject providers.
    }
    ngOnInit() {
        this.noticias.agregar("hola!");
        this.noticias.agregar("hola 2!");
        this.noticias.agregar("hola 3!");
    }
    onDrawerButtonTap() {
        const sideDrawer = _nativescript_core__WEBPACK_IMPORTED_MODULE_1__["Application"].getRootView();
        sideDrawer.showDrawer();
    }
}
SearchComponent.ɵfac = function SearchComponent_Factory(t) { return new (t || SearchComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_domian_noticias_service__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"])); };
SearchComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SearchComponent, selectors: [["Search"]], decls: 7, vars: 1, consts: [[0, "ios", "visibility", "collapsed", "icon", "res://menu", 3, "tap"], ["icon", "res://menu", 0, "android", "visibility", "collapsed", "ios.position", "left", 3, "tap"], ["text", "Search"], [1, "page__content"], ["text", "\uF002", 1, "page__content-icon", "fas"], ["class", "page__content-placeholder", 3, "text", 4, "ngFor", "ngForOf"], [1, "page__content-placeholder", 3, "text"]], template: function SearchComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ActionBar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "NavigationButton", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("tap", function SearchComponent_Template_NavigationButton_tap_1_listener() { return ctx.onDrawerButtonTap(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ActionItem", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("tap", function SearchComponent_Template_ActionItem_tap_2_listener() { return ctx.onDrawerButtonTap(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "Label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "StackLayout", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "Label", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, SearchComponent_Label_6_Template, 1, 1, "Label", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.noticias.buscar());
    } }, directives: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_3__["ActionBarComponent"], _nativescript_angular__WEBPACK_IMPORTED_MODULE_3__["NavigationButtonDirective"], _nativescript_angular__WEBPACK_IMPORTED_MODULE_3__["ActionItemDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: "Search",
                /*duleId: module.i*/
                templateUrl: "./search.component.html"
                //providers: [NoticiasService]
            }]
    }], function () { return [{ type: _domian_noticias_service__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"] }]; }, null); })();


/***/ }),

/***/ "./app/search/search.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchModule", function() { return SearchModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@nativescript/angular/__ivy_ngcc__/fesm2015/nativescript-angular.js");
/* harmony import */ var _domian_noticias_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/domian/noticias.service.ts");
/* harmony import */ var _search_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/search/search-routing.module.ts");
/* harmony import */ var _search_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/search/search.component.ts");






class SearchModule {
}
SearchModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: SearchModule });
SearchModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function SearchModule_Factory(t) { return new (t || SearchModule)(); }, providers: [_domian_noticias_service__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"]], imports: [[
            _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
            _search_routing_module__WEBPACK_IMPORTED_MODULE_3__["SearchRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SearchModule, { declarations: [_search_component__WEBPACK_IMPORTED_MODULE_4__["SearchComponent"]], imports: [_nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
        _search_routing_module__WEBPACK_IMPORTED_MODULE_3__["SearchRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                    _search_routing_module__WEBPACK_IMPORTED_MODULE_3__["SearchRoutingModule"]
                ],
                declarations: [
                    _search_component__WEBPACK_IMPORTED_MODULE_4__["SearchComponent"]
                ],
                providers: [_domian_noticias_service__WEBPACK_IMPORTED_MODULE_2__["NoticiasService"]],
                schemas: [
                    _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
                ]
            }]
    }], null, null); })();


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL3NlYXJjaC1yb3V0aW5nLm1vZHVsZS50cyIsIndlYnBhY2s6Ly8vLi9hcHAvc2VhcmNoL3NlYXJjaC5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2guY29tcG9uZW50Lmh0bWwiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NlYXJjaC9zZWFyY2gubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUM7QUFFd0I7QUFFWjs7O0FBRXJELE1BQU0sTUFBTSxHQUFXO0lBQ25CLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsaUVBQWUsRUFBRTtDQUMzQyxDQUFDO0FBTUssTUFBTSxtQkFBbUI7O2tHQUFuQixtQkFBbUI7Z0tBQW5CLG1CQUFtQixrQkFIbkIsQ0FBQyw4RUFBd0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFDMUMsOEVBQXdCO21JQUV6QixtQkFBbUIseUdBRmxCLDhFQUF3Qjs2RkFFekIsbUJBQW1CO2NBSi9CLHNEQUFRO2VBQUM7Z0JBQ04sT0FBTyxFQUFFLENBQUMsOEVBQXdCLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNwRCxPQUFPLEVBQUUsQ0FBQyw4RUFBd0IsQ0FBQzthQUN0Qzs7Ozs7Ozs7OztBQ2JEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWtEO0FBRUQ7QUFDWTs7Ozs7O0lDZ0J6RCxzRUFBcUc7OztJQUFuQixzRUFBVTs7QURSekYsTUFBTSxlQUFlO0lBRXhCLFlBQW1CLFFBQXlCO1FBQXpCLGFBQVEsR0FBUixRQUFRLENBQWlCO1FBQ3hDLHFEQUFxRDtJQUN6RCxDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2pDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxpQkFBaUI7UUFDYixNQUFNLFVBQVUsR0FBa0IsOERBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM1RCxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDNUIsQ0FBQzs7OEVBZlEsZUFBZTsrRkFBZixlQUFlO1FDWDVCLDRFQUNJO1FBSUEsc0ZBQThHO1FBQS9DLDZJQUFPLHVCQUFtQixJQUFDO1FBQUMsNERBQW1CO1FBTTlHLGdGQUVhO1FBRmdELHVJQUFPLHVCQUFtQixJQUFDO1FBRXhGLDREQUFhO1FBQ2Isc0VBQTZCO1FBQ2pDLDREQUFZO1FBRVosaUZBQ0k7UUFBQSxzRUFBOEQ7UUFDOUQsK0dBQTZGO1FBQ2pHLDREQUFjOztRQUQrQiwwREFBd0M7UUFBeEMsMEZBQXdDOzs2RkRSeEUsZUFBZTtjQU4zQix1REFBUztlQUFDO2dCQUNQLFFBQVEsRUFBRSxRQUFRO2dCQUNsQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7Z0JBQ25CLFdBQVcsRUFBRSx5QkFBeUI7Z0JBQ3RDLDhCQUE4QjthQUNqQzs7Ozs7Ozs7OztBRVZEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTJEO0FBQ007QUFDSjtBQUVDO0FBQ1Q7O0FBZTlDLE1BQU0sWUFBWTs7MkZBQVosWUFBWTtrSkFBWixZQUFZLG1CQUxWLENBQUMsd0VBQWUsQ0FBQyxZQVBuQjtZQUNMLDhFQUF3QjtZQUN4QiwwRUFBbUI7U0FDdEI7bUlBU1EsWUFBWSxtQkFQakIsaUVBQWUsYUFKZiw4RUFBd0I7UUFDeEIsMEVBQW1COzZGQVVkLFlBQVk7Y0FieEIsc0RBQVE7ZUFBQztnQkFDTixPQUFPLEVBQUU7b0JBQ0wsOEVBQXdCO29CQUN4QiwwRUFBbUI7aUJBQ3RCO2dCQUNELFlBQVksRUFBRTtvQkFDVixpRUFBZTtpQkFDbEI7Z0JBQ0QsU0FBUyxFQUFFLENBQUMsd0VBQWUsQ0FBQztnQkFDNUIsT0FBTyxFQUFFO29CQUNMLDhEQUFnQjtpQkFDbkI7YUFDSiIsImZpbGUiOiIzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJAbmF0aXZlc2NyaXB0L2FuZ3VsYXJcIjtcclxuXHJcbmltcG9ydCB7IFNlYXJjaENvbXBvbmVudCB9IGZyb20gXCIuL3NlYXJjaC5jb21wb25lbnRcIjtcclxuXHJcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xyXG4gICAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IFNlYXJjaENvbXBvbmVudCB9XHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW05hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcclxuICAgIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hSb3V0aW5nTW9kdWxlIHsgfVxyXG4iLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJhZFNpZGVEcmF3ZXIgfSBmcm9tIFwibmF0aXZlc2NyaXB0LXVpLXNpZGVkcmF3ZXJcIjtcclxuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tIFwiQG5hdGl2ZXNjcmlwdC9jb3JlXCI7XHJcbmltcG9ydCB7IE5vdGljaWFzU2VydmljZSB9IGZyb20gXCIuLi9kb21pYW4vbm90aWNpYXMuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogXCJTZWFyY2hcIixcclxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCIuL3NlYXJjaC5jb21wb25lbnQuaHRtbFwiXHJcbiAgICAvL3Byb3ZpZGVyczogW05vdGljaWFzU2VydmljZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlYXJjaENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIG5vdGljaWFzOiBOb3RpY2lhc1NlcnZpY2UpIHtcclxuICAgICAgICAvLyBVc2UgdGhlIGNvbXBvbmVudCBjb25zdHJ1Y3RvciB0byBpbmplY3QgcHJvdmlkZXJzLlxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMubm90aWNpYXMuYWdyZWdhcihcImhvbGEhXCIpO1xyXG4gICAgICAgIHRoaXMubm90aWNpYXMuYWdyZWdhcihcImhvbGEgMiFcIik7XHJcbiAgICAgICAgdGhpcy5ub3RpY2lhcy5hZ3JlZ2FyKFwiaG9sYSAzIVwiKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRyYXdlckJ1dHRvblRhcCgpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBzaWRlRHJhd2VyID0gPFJhZFNpZGVEcmF3ZXI+QXBwbGljYXRpb24uZ2V0Um9vdFZpZXcoKTtcclxuICAgICAgICBzaWRlRHJhd2VyLnNob3dEcmF3ZXIoKTtcclxuICAgIH1cclxufVxyXG4iLCI8QWN0aW9uQmFyPlxyXG4gICAgPCEtLSBcclxuICAgIFVzZSB0aGUgTmF2aWdhdGlvbkJ1dHRvbiBhcyBhIHNpZGUtZHJhd2VyIGJ1dHRvbiBpbiBBbmRyb2lkXHJcbiAgICBiZWNhdXNlIEFjdGlvbkl0ZW1zIGFyZSBzaG93biBvbiB0aGUgcmlnaHQgc2lkZSBvZiB0aGUgQWN0aW9uQmFyXHJcbiAgICAtLT5cclxuICAgIDxOYXZpZ2F0aW9uQnV0dG9uIGlvczp2aXNpYmlsaXR5PVwiY29sbGFwc2VkXCIgaWNvbj1cInJlczovL21lbnVcIiAodGFwKT1cIm9uRHJhd2VyQnV0dG9uVGFwKClcIj48L05hdmlnYXRpb25CdXR0b24+XHJcbiAgICA8IS0tIFxyXG4gICAgVXNlIHRoZSBBY3Rpb25JdGVtIGZvciBJT1Mgd2l0aCBwb3NpdGlvbiBzZXQgdG8gbGVmdC4gVXNpbmcgdGhlXHJcbiAgICBOYXZpZ2F0aW9uQnV0dG9uIGFzIGEgc2lkZS1kcmF3ZXIgYnV0dG9uIGluIGlPUyBpcyBub3QgcG9zc2libGUsXHJcbiAgICBiZWNhdXNlIGl0cyBmdW5jdGlvbiBpcyB0byBhbHdheXMgbmF2aWdhdGUgYmFjayBpbiB0aGUgYXBwbGljYXRpb24uXHJcbiAgICAtLT5cclxuICAgIDxBY3Rpb25JdGVtIGljb249XCJyZXM6Ly9tZW51XCIgYW5kcm9pZDp2aXNpYmlsaXR5PVwiY29sbGFwc2VkXCIgKHRhcCk9XCJvbkRyYXdlckJ1dHRvblRhcCgpXCJcclxuICAgICAgICBpb3MucG9zaXRpb249XCJsZWZ0XCI+XHJcbiAgICA8L0FjdGlvbkl0ZW0+XHJcbiAgICA8TGFiZWwgdGV4dD1cIlNlYXJjaFwiPjwvTGFiZWw+XHJcbjwvQWN0aW9uQmFyPlxyXG5cclxuPFN0YWNrTGF5b3V0IGNsYXNzPVwicGFnZV9fY29udGVudFwiPlxyXG4gICAgPExhYmVsIGNsYXNzPVwicGFnZV9fY29udGVudC1pY29uIGZhc1wiIHRleHQ9XCImI3hmMDAyO1wiPjwvTGFiZWw+XHJcbiAgICA8TGFiZWwgY2xhc3M9XCJwYWdlX19jb250ZW50LXBsYWNlaG9sZGVyXCIgKm5nRm9yPVwibGV0IHMgb2YgdGhpcy5ub3RpY2lhcy5idXNjYXIoKVwiIFt0ZXh0XT1cInNcIj48L0xhYmVsPlxyXG48L1N0YWNrTGF5b3V0PiIsImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlIH0gZnJvbSBcIkBuYXRpdmVzY3JpcHQvYW5ndWxhclwiO1xyXG5pbXBvcnQgeyBOb3RpY2lhc1NlcnZpY2UgfSBmcm9tIFwiLi4vZG9taWFuL25vdGljaWFzLnNlcnZpY2VcIjtcclxuXHJcbmltcG9ydCB7IFNlYXJjaFJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9zZWFyY2gtcm91dGluZy5tb2R1bGVcIjtcclxuaW1wb3J0IHsgU2VhcmNoQ29tcG9uZW50IH0gZnJvbSBcIi4vc2VhcmNoLmNvbXBvbmVudFwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgU2VhcmNoUm91dGluZ01vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIFNlYXJjaENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW05vdGljaWFzU2VydmljZV0sXHJcbiAgICBzY2hlbWFzOiBbXHJcbiAgICAgICAgTk9fRVJST1JTX1NDSEVNQVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VhcmNoTW9kdWxlIHsgfVxyXG4iXSwic291cmNlUm9vdCI6IiJ9