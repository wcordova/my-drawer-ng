import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "@nativescript/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NoticiasService } from "./domian/noticias.service";
let AppModule = class AppModule {
};
AppModule = __decorate([
    NgModule({
        bootstrap: [
            AppComponent
        ],
        imports: [
            AppRoutingModule,
            NativeScriptModule,
            NativeScriptUISideDrawerModule
        ],
        providers: [NoticiasService],
        declarations: [
            AppComponent
        ],
        schemas: [
            NO_ERRORS_SCHEMA
        ]
    })
], AppModule);
export { AppModule };
