import { Injectable } from "@angular/core";
let NoticiasService = class NoticiasService {
    constructor() {
        this.noticias = [];
    }
    agregar(s) {
        this.noticias.push(s);
    }
    buscar() {
        return this.noticias;
    }
};
NoticiasService = __decorate([
    Injectable()
], NoticiasService);
export { NoticiasService };
