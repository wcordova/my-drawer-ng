import { Component } from "@angular/core";
import { Application } from "@nativescript/core";
let FeaturedComponent = class FeaturedComponent {
    constructor() {
        // Use the component constructor to inject providers.
    }
    ngOnInit() {
        // Init your component properties here.
    }
    onDrawerButtonTap() {
        const sideDrawer = Application.getRootView();
        sideDrawer.showDrawer();
    }
};
FeaturedComponent = __decorate([
    Component({
        selector: "Featured",
        templateUrl: "./featured.component.html"
    }),
    __metadata("design:paramtypes", [])
], FeaturedComponent);
export { FeaturedComponent };
