import { Component } from "@angular/core";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domian/noticias.service";
let SearchComponent = class SearchComponent {
    constructor(noticias) {
        this.noticias = noticias;
        // Use the component constructor to inject providers.
    }
    ngOnInit() {
        this.noticias.agregar("hola!");
        this.noticias.agregar("hola 2!");
        this.noticias.agregar("hola 3!");
    }
    onDrawerButtonTap() {
        const sideDrawer = Application.getRootView();
        sideDrawer.showDrawer();
    }
};
SearchComponent = __decorate([
    Component({
        selector: "Search",
        moduleId: module.id,
        templateUrl: "./search.component.html"
        //providers: [NoticiasService]
    }),
    __metadata("design:paramtypes", [NoticiasService])
], SearchComponent);
export { SearchComponent };
