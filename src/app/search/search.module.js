import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { NoticiasService } from "../domian/noticias.service";
import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
let SearchModule = class SearchModule {
};
SearchModule = __decorate([
    NgModule({
        imports: [
            NativeScriptCommonModule,
            SearchRoutingModule
        ],
        declarations: [
            SearchComponent
        ],
        providers: [NoticiasService],
        schemas: [
            NO_ERRORS_SCHEMA
        ]
    })
], SearchModule);
export { SearchModule };
