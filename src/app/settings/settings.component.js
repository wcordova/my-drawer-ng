import { Component } from "@angular/core";
import { Application } from "@nativescript/core";
let SettingsComponent = class SettingsComponent {
    constructor() {
        // Use the component constructor to inject providers.
    }
    ngOnInit() {
        // Init your component properties here.
    }
    onDrawerButtonTap() {
        const sideDrawer = Application.getRootView();
        sideDrawer.showDrawer();
    }
};
SettingsComponent = __decorate([
    Component({
        selector: "Settings",
        templateUrl: "./settings.component.html"
    }),
    __metadata("design:paramtypes", [])
], SettingsComponent);
export { SettingsComponent };
