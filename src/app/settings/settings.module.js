import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { SettingsRoutingModule } from "./settings-routing.module";
import { SettingsComponent } from "./settings.component";
let SettingsModule = class SettingsModule {
};
SettingsModule = __decorate([
    NgModule({
        imports: [
            NativeScriptCommonModule,
            SettingsRoutingModule
        ],
        declarations: [
            SettingsComponent
        ],
        schemas: [
            NO_ERRORS_SCHEMA
        ]
    })
], SettingsModule);
export { SettingsModule };
